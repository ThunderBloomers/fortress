﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace The_Fortress
{
    class Program
    {
        public const int FORWARD = 1;
        public const int BACKWARD = 2;
        public const int LEFT = 3;
        public const int RIGHT = 4;
        public const int WRONG = 0;

        static void Main(string[] args)
        {
            beginGame();
        }
        static void debuggin()
        {
            fallOffCliff();
        }
        static void beginGame()
        {

            write("Welcome.\n\n");
            write("You have just entered a wooded area near your house. You see a bright flash. Suddenly, you find yourself on a precipice in front of a large stone fortress.");
            int keyInput = readKeyInput();

            if (keyInput != FORWARD)
            {
                fallOffCliff();
            }

            else
            {
                enterFortress();
            }
            

        }
        static int readKeyInput()
        {
            int returnValue = WRONG;
            string text = "WRONG";

            ConsoleKeyInfo input = Console.ReadKey();
            ConsoleKey key = input.Key;

            if (key == ConsoleKey.UpArrow)
            {
                returnValue = FORWARD;
                text = "MOVE FORWARD";
            }
            else if (key == ConsoleKey.DownArrow)
            {
                returnValue = BACKWARD;
                text = "MOVE BACKWARD";
            }
            else if (key == ConsoleKey.LeftArrow)
            {
                returnValue = LEFT;
                text = "MOVE LEFT";
            }
            else if (key == ConsoleKey.RightArrow)
            {
                returnValue = RIGHT;
                text = "MOVE RIGHT";
            }

            /***************************
             * 
             *  DEBUG ONLY
             *
             * ************************/
            else if(key == ConsoleKey.D)
            {
                debuggin();
            }
            

            write(text);

            return returnValue;
        }

        static void fallOffCliff()
        {
            write("You have fallen off of a cliff and died. Oh well.");
            endGame();
        }
        static void endGame()
        {
            sleepThread(10000);
            Environment.Exit(0);
        }
        static void sleepThread(int time)
        {
            System.Threading.Thread.Sleep(time);
        }
        static void write(string text, int tabSize = 8)
        {

            text = "\n\t" + text;

            string[] lines = text
                .Replace("\t", new String(' ', tabSize))
                .Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            for (int i = 0; i < lines.Length; i++)
            {
                string process = lines[i];
                List<String> wrapped = new List<string>();

                while (process.Length > Console.WindowWidth)
                {
                    int wrapAt = process.LastIndexOf(' ', Math.Min(Console.WindowWidth - 1, process.Length));
                    if (wrapAt <= 0) break;

                    wrapped.Add(process.Substring(0, wrapAt));
                    process = process.Remove(0, wrapAt + 1);
                }

                foreach (string wrap in wrapped)
                {
                    Console.WriteLine(wrap);
                }

                Console.WriteLine(process);
            }
        }
        static void enterFortress()
        {
            write("You have entered the fortress");
            wonGame();
        }

        static void wonGame()
        {
            write("You trip on a rock and stumble into a room with only one feature: a pedastal in the center. It is glowing.");

            int move = readKeyInput();

            if (move == FORWARD)
            {
                int chance = getRandom(37);

                if (chance == 37)
                {
                    write("The light feels harmonious and warm - you fade out, but reappear in the wooded area near your house. Your primary checking account now has 37 million dollars in it. Neat.");
                    endGame();
                }
                else if (chance % 3 == 0)
                {
                    write("You feel the light pull you away from the fortress. You appear back in the wooded area near your house. You are not dead. It is a result.");
                    endGame();
                }
                else
                {
                    write("The light is cold and resonant. You are ripped away from the fortress and thrown ethereally in evey direction. You find yourself back in your wooded area. You step on a twig wrong and stumble backwards. Turns out, there's a cliff there...");
                    sleepThread(7000);
                    Console.Write("\n\t.");
                    sleepThread(2000);
                    Console.Write(".");
                    sleepThread(1000);
                    Console.Write(".");
                    sleepThread(1000);
                    Console.Write("\n");

                    fallOffCliff();
                }
            }
            else if (move == BACKWARD)
            {
                gameMove(BACKWARD);
            }
            else
            {
                write("There were spikes on that wall. You died. Oh well.");
                endGame();
            }


        }
        static int getRandom(int x)
        {
            Random rnd = new Random();
            return rnd.Next(1, x+1);
        }
        static void gameMove(int move)
        {
            write("This ain't implemented yet.");
            fallOffCliff();
        }

    }
}
